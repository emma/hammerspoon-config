-----------------------------------------------
-- Globally used functions
-----------------------------------------------

function string.urlEncode( str )
   if ( str ) then
      str = string.gsub( str, "\n", "\r\n" )
      str = string.gsub( str, "([^%w ])",
         function (c) return string.format( "%%%02X", string.byte(c) ) end )
      str = string.gsub( str, " ", "+" )
   end
   return str
end

-----------------------------------------------
-- Setup
-----------------------------------------------

local hyper = {"shift", "cmd", "alt", "ctrl"}
hs.window.animationDuration = 0

-- Grid settings
hs.grid.ui.fontName = ".Helvetica Neue DeskInterface UltraLight"
hs.grid.ui.cellColor = {1,1,1,0.3}
hs.grid.ui.selectedColor = {0.1725490196,0.5882352941,0.9254901961,0.3}
hs.grid.ui.highlightColor = {0,0,0,0}
hs.grid.ui.highlightStrokeColor = {0.1725490196,0.5882352941,0.9254901961,1}
hs.grid.ui.textColor = {0,0,0,0.9}
hs.grid.ui.highlightStrokeWidth = 11
hs.grid.ui.cellStrokeWidth = 1
hs.grid.ui.textSize = 78
hs.grid.ui.padding = 30
function updateScreenFrame()
    mainGrid = hs.screen.mainScreen()
    gridFrame = hs.geometry.rect(
        mainGrid:frame().x + 4, 
        mainGrid:frame().y + 4, 
        mainGrid:frame().w - 12, 
        mainGrid:frame().h - 3
    )
    hs.grid.setGrid('6x4', mainGrid, gridFrame).setMargins(hs.geometry.size(8, 8))
    print('Grid updated')
end
hs.screen.watcher.newWithActiveScreen(updateScreenFrame):start()
updateScreenFrame()
hs.grid.ui.showExtraKeys = false

-----------------------------------------------
-- Load browser list
-----------------------------------------------

local open = io.open

local function read_file(path)
    local file = open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

browsers = hs.json.decode(read_file("browsers.json"))

-- Populate browsers with images
for k,v in pairs(browsers["browserPossibilities"]) do
    browsers["browserPossibilities"][k]["image"] = 
        hs.image.imageFromAppBundle(browsers["browserPossibilities"][k]["id"])
end

-----------------------------------------------
-- Handle HTTP/HTTPS URLs
-----------------------------------------------

-- Load extensions beforehand so that they aren't 
-- reloaded every time a URL is opened
local hschooser = hs.chooser
local hshost = hs.host
local hsapplication = hs.application
local hseventtap = hs.eventtap

function openInBrowser(selectedBrowser, fullURL)
    hs.urlevent.openURLWithBundle(fullURL, selectedBrowser.id)
end

function hs.urlevent.httpCallback(scheme, host, params, fullURL)

    -- Required for hs.chooser (callback)
    function doOpenBrowser(selectedBrowser)
        openInBrowser(selectedBrowser, fullURL)
    end

    local browser = nil

    -- Check for running browsers
    for index, value in pairs(browsers["preferredBrowsers"]) do
        if hsapplication.get(value["id"]) then
            browser = value
            break
        end
    end

    local optHeld = hseventtap.checkKeyboardModifiers()["fn"]

    -- If none are running then show the chooser
    if (not browser) or optHeld then
        -- Get running states of browsers
        if optHeld then
            for k,v in pairs(browsers["browserPossibilities"]) do
                if hsapplication(browsers["browserPossibilities"][k]["id"]) then
                    browsers["browserPossibilities"][k]["subText"] = "Running"
                end
            end
        else
            for k,v in pairs(browsers["browserPossibilities"]) do
                browsers["browserPossibilities"][k]["subText"] = nil
            end
        end

        -- Init chooser
        local chooser = hschooser.new(doOpenBrowser)
                    :choices(browsers["browserPossibilities"])
                    :width(30)
                    :fgColor({
                        ["red"] = 0,
                        ["green"] = 0,
                        ["blue"] = 0,
                        ["alpha"] = 0.87
                    })
                    :subTextColor({
                        ["red"] = 0,
                        ["green"] = 0,
                        ["blue"] = 0,
                        ["alpha"] = 0.54
                    })

        -- Check OS X dark mode
        if hshost.interfaceStyle() == "Dark" then
            chooser:bgDark(true)
                :fgColor({
                    ["red"] = 1.0,
                    ["green"] = 1.0,
                    ["blue"] = 1.0,
                    ["alpha"] = 0.87
                })
                :subTextColor({
                    ["red"] = 1.0,
                    ["green"] = 1.0,
                    ["blue"] = 1.0,
                    ["alpha"] = 0.54
                })
        else
            chooser:bgDark(false)
                :fgColor({
                    ["red"] = 0,
                    ["green"] = 0,
                    ["blue"] = 0,
                    ["alpha"] = 0.87
                })
                :subTextColor({
                    ["red"] = 0,
                    ["green"] = 0,
                    ["blue"] = 0,
                    ["alpha"] = 0.54
                })
        end

        chooser:show() -- Since the URL is opened within the callback,
                       -- there's no need to open it explicitly
    else
        openInBrowser(browser, fullURL)
    end
end

-----------------------------------------------
-- Reload config on write/edit
-----------------------------------------------

-- hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", hs.reload):start()


-----------------------------------------------
-- Keybindings
-----------------------------------------------

-- cmd+alt+tab to show window hints
hs.hotkey.bind({"cmd", "alt"}, 'tab', function()
    hs.hints.windowHints()
end)

-- Hyper+hjkl to switch window focus
hs.hotkey.bind(hyper, 'k', function()
    if hs.window.focusedWindow() then
        hs.window.focusedWindow():focusWindowNorth()
    else
        hs.alert.show("No active window")
    end
end)

hs.hotkey.bind(hyper, 'j', function()
    if hs.window.focusedWindow() then
        hs.window.focusedWindow():focusWindowSouth()
    else
        hs.alert.show("No active window")
    end
end)

hs.hotkey.bind(hyper, 'l', function()
    if hs.window.focusedWindow() then
    hs.window.focusedWindow():focusWindowEast()
    else
        hs.alert.show("No active window")
    end
end)

hs.hotkey.bind(hyper, 'h', function()
    if hs.window.focusedWindow() then
        hs.window.focusedWindow():focusWindowWest()
    else
        hs.alert.show("No active window")
    end
end)

-- Shift-Opt-Cmd+G to show grid
hs.hotkey.bind({"shift", "cmd", "alt"}, 'g', function()
    if hs.window.focusedWindow() then
        updateScreenFrame()
        hs.grid.toggleShow()
    else
        hs.alert.show("No active window")
    end
end)

-- Ctrl-Opt-Cmd+T to open iTerm2
hs.hotkey.bind({"cmd", "alt", "ctrl"}, 't', function()
    hs.application.launchOrFocus("iTerm")
end)

-- Free disk space monitor
hasFreeSpaceNotificationSent = false
local minimumFreeSpace = 20

function openFreeDiskSpaceCallback(notification)
    -- body
end

freeSpaceNotifier = hs.notify.new(openFreeDiskSpaceCallback, {title="Low disk space!", alwaysPresent=true}):setIdImage("~/.hammerspoon/ProblemReporter.icns")
-- contentImage=hs.image.imageFromPath("~/.hammerspoon/ProblemReporter.icns")

function checkFreeSpaceAndNotify()
    local freeSpace = hs.host.volumeInformation()["/"].NSURLVolumeAvailableCapacityKey / 1000000000
    if freeSpace < minimumFreeSpace then
        if not hasFreeSpaceNotificationSent then
            freeSpaceNotifier:withdraw():informativeText("Less than " .. minimumFreeSpace .. " GB free."):send()
            hasFreeSpaceNotificationSent = true
        end
    else
        hasFreeSpaceNotificationSent = false
    end
end

freeSpaceNotifierTimer = hs.timer.doEvery(60, checkFreeSpaceAndNotify)
notificationSentFlagResetter = hs.timer.doEvery(3600, function() hasFreeSpaceNotificationSent = false end)

